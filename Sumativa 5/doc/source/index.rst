.. Plataforma para verificar placas documentation master file, created by
   sphinx-quickstart on Sun Jul 21 02:38:10 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenido al Sistema de Verificación de placas online
===========================================================
.. image:: _static/att.jpg
    :align: center
    :alt: Placa de Panamá


*Plataforma para verificar placas en la República de Panamá* 
Con este moderno sistema podemos verificar el tipo de placa de los automóviles que circulan en el territorio nacional.
Es importante saber que las placas en nuestro país tienen 6 dígitos y letras que las identifican.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. automodule:: transito_pkg.placa
    :members:
    :special-members:
    :undoc-members:

Indices y tablas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
