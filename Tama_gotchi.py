import random


class Tama_gotchi:
    def __init__(self, nomb):
        gen = random.choice(["M", "F"])
        print("Su Tamagotchi es " + gen)
        self.gen = gen
        self.nomb = nomb
        self.hamb = 10
        self.felic = 10
        self.salud = 0

    def evento(self, accion):
        self.hamb -= 1
        self.felic -= 1
        if accion == "alimentar":
            self.alimen()
        elif accion == "jugar":
            self.jugar()

    def alimentar(self):
        print("Has alimentado a " + self.nomb)
        self.hamb += 2
        if self.hamb > 10:
            # Sobrealimentado
            self.salud += 2

        pass

    def jugar(self):
        print("Juegas con " + self.nomb)
        self.felic += 3
        # Evitar sobrepasar
        if self.felic > 10:
            self.felic = 10

    def muere(self):
        if self.salud >= 10 or self.felic <= 0 or self.hamb <= 0:
            return True
        else:
            return False

    def imprimir_estado(self):
        print("Hambre: " + str(self.hamb))
        print("Felicidad: " + str(self.felic))
        print("Salud: " + str(self.salud))


def main():
    nomb = input("Digite el nombre de tu Tamagotchi: ")
    tmagotchi = Tama_gotchi(nomb)
    while not tmagotchi.muere():
        tmagotchi.imprimir_estado()
        eleccion = ""
        while not eleccion in ["1", "2", "3"]:
            eleccion = input("""
1. Alimentar
2. Jugar
3. Nada
Elige: """)
        if eleccion == "1":
            tmagotchi.evento("alimen")
        elif eleccion == "2":
            tmagotchi.evento("jugar")
        elif eleccion == "3":
            tmagotchi.evento("")
    print(tmagotchi.nomb + " ha muerto")


main()
